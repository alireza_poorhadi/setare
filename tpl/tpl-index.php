<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= SITE_TITLE ?></title>
    <link href="<?= BASE_URL ?>assets/img/favicon.png" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/styles.css">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
</head>

<body>
    <div class="main">
        <div class="head">
            <div class="search-box">
                <input type="text" id="search" placeholder="دنبال کجا می گردی؟">
                <div class="clear"></div>
                <div class="search-results" style="display: none;">
                    <!-- <div class="result-item" data-lat='111' data-lng='222'>
                        <span class="loc-type">رستوران</span>
                        <span class="loc-title">رستوران و قوه خانه سنتی سون لرن</span>
                    </div> -->
                
                </div>
            </div>
        </div>
        <div class="mapContainer">
            <div id="map"></div>
        </div>
    </div>

    <div class="modal-overlay" style="display: none;">
        <div class="modal">
            <span class="close">x</span>
            <h3 class="modal-title">ثبت مکان</h3>
            <div class="modal-content">
                <form id='addLocationForm' action="<?= BASE_URL . 'process/location.php' ?>" method="post">
                    <div class="field-row">
                        <div class="field-title">مختصات</div>
                        <div class="field-content">
                            <input type="text" name='lat' id="lat-display" readonly style="width: 160px;text-align: center;">
                            <input type="text" name='lng' id="lng-display" readonly style="width: 160px;text-align: center;">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="field-title">نام مکان</div>
                        <div class="field-content">
                            <input type="text" name="title" id='l-title' placeholder="مثلاً: رستوران علیرضا خان">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="field-title">نوع</div>
                        <div class="field-content">
                            <select name="type" id='l-type'>
                                <?php foreach (locationTypes as $key => $loc) : ?>
                                    <option value="<?= $key ?>"><?= $loc ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="field-title">&nbsp;&nbsp;</div>
                        <div class="field-content">
                            <input type="submit" value=" ثبت ">
                        </div>
                    </div>
                    <div id="ajaxResult"></div>
                </form>
            </div>
        </div>
    </div>

</body>
<script src="<?= BASE_URL ?>assets/js/jquery.min.js"></script>
<script src="<?= BASE_URL ?>assets/js/myJs.js"></script>
<script>
    <?php if ($location) : ?>
        L.marker([<?= $location->lat ?>, <?= $location->lng ?>]).addTo(map).bindPopup("<?= $location->title ?>").openPopup();
    <?php endif; ?>
</script>

</html>