const defaultLocation = [32.6464892, 51.3433138];
const defaultZoom = 13;

var map = L.map('map').setView(defaultLocation, defaultZoom);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);

document.getElementById('map').style.setProperty('height', window.innerHeight + "px");

getAllLocations();

map.on('moveend', function(e) {
    //getAllLocations();
    console.log(e);
});

map.on('zoomend', function(e) {
    //getAllLocations();
    //console.log(e);
});

map.on('dblclick', function(e) {
    L.marker(e.latlng).addTo(map);
    $('.modal-overlay').fadeIn();
    $('#lat-display').val(e.latlng.lat);
    $('#lng-display').val(e.latlng.lng);
});

function getAllLocations() {
    var northLine = map.getBounds().getNorth();
    var westLine = map.getBounds().getWest();
    var southLine = map.getBounds().getSouth();
    var eastLine = map.getBounds().getEast();
    $.ajax({
        url: 'process/searchAll.php',
        type: 'POST',
        data: {
            nl: northLine,
            sl: southLine,
            el: eastLine,
            wl: westLine
        },
        success: function(r) {
            var myArr = JSON.parse(r);
            $.each(myArr, function(index, val) {
                L.marker([val.lat, val.lng]).addTo(map).bindPopup(val.title).openPopup();
            });
        }
    });
}

$(document).ready(function() {
    $('.modal-overlay .close').click(function() {
        $('.modal-overlay').fadeOut();
        $("#ajaxResult").html('');
        $('#l-title').val('');
        $('#l-type').val(0);
    });

    $('#addLocationForm').on('submit', function(e) {
        var frm = $(this);
        e.preventDefault();
        $.ajax({
            url: frm.attr('action'),
            type: frm.attr('method'),
            data: frm.serialize(),
            success: function(r) {
                $('#ajaxResult').html(r);
            }
        });
    });

    $('input#search').keyup(function() {
        const input = $(this);
        const searchResult = $('.search-results');
        searchResult.html('در حال جستجو ... ');
        $.ajax({
            url: 'process/search.php',
            type: 'POST',
            data: { keyword: input.val() },
            success: function(r) {
                searchResult.slideDown().html(r);
            }
        });
    });
});