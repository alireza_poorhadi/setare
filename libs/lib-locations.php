<?php
defined('BASE_PATH') or die("Permission Denied.");

function addLocation($data) {
      global $conn;
      $sql = "INSERT INTO locations (title, lat, lng, type) VALUES (:title, :lat, :lng, :typ)";
      $stmt = $conn->prepare($sql);
      $stmt->execute(['title' => $data['title'], 'lat' => $data['lat'], 'lng' => $data['lng'], 'typ' => $data['type']]);
      return $stmt->rowCount();
}

function getLocations($params = [])
{
      global $conn;
      $condition = '';
      if(isset($params['verified']) and in_array($params['verified'],['0', '1'])) {
            $condition = "WHERE verified = {$params['verified']}";
      } else if (isset($params['keyword'])) {
            $condition = "WHERE verified = 1 AND title LIKE '%{$params['keyword']}%'";
      }
      $sql = "SELECT * FROM locations {$condition}";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_OBJ);
}

function getLocation($id)
{
      global $conn;
      $sql = "SELECT * FROM locations WHERE id = :id";
      $stmt = $conn->prepare($sql);
      $stmt->execute(['id' => $id ]);
      return $stmt->fetch(PDO::FETCH_OBJ);
}

function toggleStatus($id)
{
      global $conn;
      $sql = "UPDATE locations SET verified = 1 - verified WHERE id = :id";
      $stmt = $conn->prepare($sql);
      $stmt->execute(['id' => $id ]);
      return $stmt->rowCount();
}

function getAllLocations($data)
{
      global $conn;
      
      $sql = "SELECT * FROM locations WHERE verified = 1 AND (lat BETWEEN {$data['sl']} AND {$data['nl']}) AND (lng BETWEEN {$data['wl']} AND {$data['el']}) ";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_OBJ);
}