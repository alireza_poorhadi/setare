<?php
include_once 'bootstrap/init.php';

$locId = $_GET['loc'] ?? null;
$location = false;
if(isset($locId) and is_numeric($locId)) {
      $location = getLocation($locId);
}

include_once 'tpl/tpl-index.php';