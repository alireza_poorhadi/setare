<?php
include_once '../bootstrap/init.php';

if(!isAjaxRequest()) {
      diePage("Invalid Request");
}

$results = json_encode(getAllLocations($_POST));
echo $results;