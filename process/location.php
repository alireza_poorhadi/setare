<?php
include_once '../bootstrap/init.php';

if(!isAjaxRequest()) {
      diePage("Invalid Request");
}


if(addLocation($_POST)) {
      echo "<span style='color: green;'>مکان با موفقیت ثبت شد و منتظر تایید است. </span>";
} else {
      echo "<span style='color: red;'>خطایی در روند ثبت مکان روی داد!</span>";
}