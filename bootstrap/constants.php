<?php
function url(){
      if(isset($_SERVER['HTTPS'])){
          $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
      }
      else{
          $protocol = 'http';
      }
      return $protocol . "://" . $_SERVER['HTTP_HOST'];
  }

define("SITE_TITLE", "ستاره، نقشه");
define("BASE_URL", url()."/setare/");
define("BASE_PATH", $_SERVER['DOCUMENT_ROOT']."/setare/");

const locationTypes = [
    0 => "عمومی",
    1 => "رستوران",
    2 => "کتابخانه",
    3 => "دانشگاه",
    4 => "پمپ بنزین",
    5 => "خصوصی",
    6 => "پارک",
    7 => "فروشگاه",
    8 => "پاساژ",
    9 => "کافی شاپ"
];