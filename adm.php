<?php
include_once 'bootstrap/init.php';

if(isset($_GET['logout']) and $_GET['logout'] == 1) {
      logout();
}

$msg = null;
if($_SERVER['REQUEST_METHOD'] == "POST") {
      if(!login($_POST['username'], $_POST['username'])) {
            $msg = "نام کاربری یا رمز عبور نادرست است!";
      }
}

if(isLoggedin()) {
      $params = $_GET ?? [];
      $locations = getLocations($params);
      include_once 'tpl/tpl-adm.php';
} else {
      include_once 'tpl/tpl-adm-form.php';
}